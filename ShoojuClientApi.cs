﻿using Shooju;
using Shooju.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoojuClient
{
    public class ExnendedPoint
    {
        public string RicId { get; set; }
        public string InstrumentName { get; set; }
        public string LotUnits { get; set; }
        public string Description { get; set; }
        public string DssValue { get; set; }
        public double? AvgPrice { get; set; }
        public DateTime? PriceDate { get; set; }
        public double? Mmbtu { get; set; }
    }

    public class ShoojuClientApi
    {
        public IEnumerable<ExnendedPoint> GetComparativeFuelPrices(string ricIds, DateTime fromDate, DateTime toDate)
        {
            if (string.IsNullOrWhiteSpace(ricIds))
            {
                ricIds = "CQCSXC1,HO-NYH,FO03-H-NYH,FO1-H-NYH,HO-USG,FO07-L-USG,FO3-USG,WTC-";
            }
            var df = ((long)fromDate.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds).ToString(); // from date 
            var dt = ((long)toDate.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds).ToString(); //end date 
            var sj = new Connection("https://eig.shooju.com", "ssathyanarayana", "6EHFi2GL1Zl18dUd5fgYncTqNVyXuXQKEOok56l1N9eysjKQdkZuOzAsEbDTu6bBV");

            //var fields = sj.Scroll("sid:reuters ric_id=(CQCSXc1,HO-NYH,HO-USG,FO07-L-USG,FO3-USG,WTC-) corrected_by=rro", new List<string> { "*" }, df: "-7d", maxPoints: -1).SelectMany(x => x.Points.Select(y => new ExnendedPoint(y.Date, y.Value) { RicId = x.Fields["ric_id"].ToString(), JobId = y.JobId, Timestamp = y.Timestamp })).ToArray();
            var fields = sj.Scroll(string.Format("sid:reuters dss_value:Close Price ric_id=({0})", ricIds), new List<string> { "instrument_name", "lot_units", "ric_id", "description" }, df: df, dt: dt, maxPoints: -1).Select(x =>
            new ExnendedPoint
            {
                RicId = x.Fields.ContainsKey("ric_id") ? x.Fields["ric_id"].ToString() : string.Empty,
                Description = x.Fields.ContainsKey("description") ? x.Fields["description"].ToString() : string.Empty,
                InstrumentName = x.Fields.ContainsKey("instrument_name") ? x.Fields["instrument_name"].ToString() : string.Empty,
                LotUnits = x.Fields.ContainsKey("lot_units") ? x.Fields["lot_units"].ToString().ToLower().Trim() : default(string),
                DssValue = x.Fields.ContainsKey("dss_value") ? x.Fields["dss_value"].ToString() : string.Empty,
                AvgPrice = x.Points != null && x.Points.Any() ? x.Points.Average(v => v.Value) : default(double?)
            }).ToArray();
            foreach (var f in fields)
            {
                if (f.AvgPrice.HasValue)
                {
                    var lotUnits = f.LotUnits;
                    switch (lotUnits)
                    {
                        case "u gal":
                            f.Mmbtu = (f.AvgPrice * 42) / 582.5;
                            f.LotUnits = f.LotUnits.TrimStart('u').Trim();
                            break;
                        case "bbl":
                            f.Mmbtu = f.AvgPrice / 6.287; break;
                        case "tons":
                            f.Mmbtu = f.AvgPrice / 5.8;
                            f.LotUnits = f.LotUnits.TrimEnd('s');
                            break;
                        default: break;
                    }
                }
            }
            return fields;
        }

        public string GetComparativeFuelPrices2(string ricIds, DateTime fromDate, DateTime toDate)
        {
            return string.Format("{0} - {1} - {2}", fromDate, toDate, ricIds);
        }
    }
}
