﻿using Shooju;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoojuClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //var k = new ShoojuClientApi().GetComparativeFuelPrices("", DateTime.Now.AddDays(-7), DateTime.Now);
            var df = ((long)(new DateTime(1996, 03, 25).Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds)).ToString(); // from date 
            var dt = ((long)(new DateTime(1996, 03, 25).AddDays(7).Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds)).ToString(); //end date 
            var sj = new Connection("https://eig.shooju.com", "ssathyanarayana", "6EHFi2GL1Zl18dUd5fgYncTqNVyXuXQKEOok56l1N9eysjKQdkZuOzAsEbDTu6bBV");
            //var sj = new Connection("https://eig.shooju.com", "api.datawarehouse", "Yib8U9AdYJIR6RD50mPPQByy7eLnbJcua0Xqy0Zk8ex7GPjyvKWYioed9Utvxxoxx");
            //var fields = sj.Scroll("sid:reuters dss_value:Mid Price ric_id=(CQCSXc1,HO-NYH,HO-USG,FO07-L-USG,FO3-USG,WTC-)  @A:w corrected_by=rro", new List<string> { "*" }, df: "-7d", maxPoints: -1).SelectMany(x => x.Points.Select(y => new ExnendedPoint(y.Date, y.Value) { RicId = x.Fields["ric_id"].ToString(), JobId = y.JobId, Timestamp = y.Timestamp })).ToArray();
            var fields = sj.Scroll("sid:reuters dss_value:(\"Close Price\", \"Mid Price\") ric_id=(CQCSXc1,HO-NYH,FO03-H-NYH,FO1-H-NYH,HO-USG,FO07-L-USG,FO3-USG,WTC-) @A:w", new List<string> { "instrument_name", "lot_units", "ric_id", "dss_value", "description" }, df: df, dt: dt, maxPoints: -1)/*.Where(x=>x.SeriesId.EndsWith("close_price"))*/.Select(x =>
            new ExnendedPoint
            {
                RicId = x.Fields.ContainsKey("ric_id") ? x.Fields["ric_id"].ToString(): string.Empty,
                Description = x.Fields.ContainsKey("description") ? x.Fields["description"].ToString() : string.Empty,
                InstrumentName = x.Fields.ContainsKey("instrument_name") ? x.Fields["instrument_name"].ToString() : string.Empty,
                LotUnits = x.Fields.ContainsKey("lot_units") ? x.Fields["lot_units"].ToString().ToLower().Trim() : default(string),
                DssValue = x.Fields.ContainsKey("dss_value") ? x.Fields["dss_value"].ToString() : string.Empty,
                AvgPrice = x.Points != null && x.Points.Any() ? x.Points.Average(v=>v.Value): default(double?),
                PriceDate = x.Points != null && x.Points.Any() ? x.Points.Max(v => v.Date) : default(DateTime?)
            }).ToArray();
            foreach (var f in fields)
            {
                if (f.AvgPrice.HasValue)
                {
                    var lotUnits = f.LotUnits;
                    switch (lotUnits)
                    {
                        case "u gal":
                            f.Mmbtu = (f.AvgPrice * 42) / 582.5;
                            f.LotUnits = f.LotUnits.TrimStart('u').Trim();
                            break;
                        case "bbl":
                            f.Mmbtu = f.AvgPrice / (f.RicId == "WTC-" ? 5.8 : 6.287); break;
                        case "tons":
                            f.Mmbtu = f.AvgPrice / 24.05;
                            f.LotUnits = f.LotUnits.TrimEnd('s');
                            break;
                        default:break;
                    }
                }
            }

            Console.ReadLine();
            
        }

    }
}
